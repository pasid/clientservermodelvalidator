package br.socket.domain;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Client {

	private Socket clientSocket;
	private Integer port;
	private String ip; 
	private String filePath;

//	public static void main(String[] args) throws UnknownHostException, IOException {
//		System.err.println("CLIENT");
//		Client cli = new Client("localhost",123, "dafaf");
//		cli.execute(args[0]);
//	}
	
	public Client(String ip, Integer port, String filePath) {
		this.ip = ip;
		this.port = port;
		this.filePath = filePath;
	}

	public void execute() throws FileNotFoundException, IOException {
		createConnection();
		enviarArquivo();
	}



	private void createConnection() throws IOException {
		clientSocket = new Socket(ip, port);
	}

	private void enviarArquivo() throws IOException {
		BufferedReader arqBuffer;
		ArrayList<String> linhasArray = new ArrayList<>();
		String linha;
		

		arqBuffer = new BufferedReader(new FileReader(this.filePath));
		linha = arqBuffer.readLine() + "\n";
		linhasArray.add(linha);

		while (linha != null) {
			linha = arqBuffer.readLine();
			linhasArray.add(linha);
		}
		arqBuffer.close();

		linhasArray.remove(linhasArray.size() - 1);
		linha = linhasArray.get(0);

		for (int i = 1; i < linhasArray.size(); i++) {
			linha += linha = linhasArray.get(i) + "\n";
		}

		DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
		dataOutputStream.writeBytes(linha);
		dataOutputStream.flush();

		
		
		DataInputStream c = new DataInputStream(clientSocket.getInputStream());
	    System.err.println("The result from server was: \n" + readData());
	    
	    dataOutputStream.close();
	}
	
	private String readData() {
	    try {
	    InputStream is = clientSocket.getInputStream();
	    ObjectInputStream ois = new ObjectInputStream(is);
	     return (String)ois.readObject();
	    } catch(Exception e) {
	        return null;
	    }
	}
	
	@Override
	public String toString() {
		return "CLIENT CONFIGURED WITH ... server ip: " + this.ip +" server port: " + this.port;
	}

}
