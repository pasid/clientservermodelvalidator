package br.socket.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * This class is responsible by managing the pool of clients which send files to be processed by servers.
 * Each client is configured to send a request to an specific server.
 * The ip and port of the servers are disposed into the file server_ips.properties.
 * The inSequenceExecution means that only one server will be available at a time --- this is equivalent to have only one available server.
 * @author Airton
 *
 */
public class ClientManager {

	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		multithreadingExecution(args[0], args[1]);
		
		System.out.println("Total Time (ms): " + (System.currentTimeMillis() - start));
	}
	

	private static void multithreadingExecution(String filePath, String propertiesFile) {
		CountDownLatch latch;
		try {
			List<Client> a = getClients(filePath, propertiesFile);
			latch = new CountDownLatch(a.size());
			for (Client client : a) {
				(new ThreadClient(latch, client)).start();
			}
			
			  try {
		            latch.await();  
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        System.out.println("All Threads Completed.");
		        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static List<Client> getClients(String filePath,String propertiesFile) throws IOException {
		List<Client> clients = new ArrayList<Client>();
	  File file = new File(propertiesFile); 
	  BufferedReader br = new BufferedReader(new FileReader(file)); 
	  
	  String st; 
	  while ((st = br.readLine()) != null && st.contains(";")) {
			  String port = st.split(";")[1];
			Integer p = new Integer(port);
			clients.add(new Client(st.split(";")[0], p,filePath));
	  }
	  
	  return clients;
	  }
	
}
